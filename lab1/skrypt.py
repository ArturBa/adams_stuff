import numpy as np
import matplotlib.mlab as ml
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random
import math
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


OBSTACLES_AMOUNT = 5
SIZE = 10
delta = 0.1

k_oi = 1
d_0 = 20
k_p = 100


class Point:
    def __init__(self, x_external, y_external):
        # round required because problems with -9.7+0.1 = -9.6000001
        self.x = round(x_external, 10)
        self.y = round(y_external, 10)

    def norm(self):
        return math.sqrt(math.pow(self.x, 2) + math.pow(self.y, 2))

    def diff(self, temp_vect):
        x = self.x - temp_vect.x
        y = self.y - temp_vect.y
        return Point(x, y)

    def get(self):
        return self.x, self.y

    def __add__(self, other):
        """
        Override + function for Point
        :param other: [Point] Other point to add
        :return: [Point] Sum of self and other
        """
        x = self.x + other.x
        y = self.y + other.y
        return Point(x, y)

    def __sub__(self, other):
        """
        Override - function for Point
        :param other: [Point] Point to subtract
        :return: [Point] Diff of self and other
        """
        x = self.x - other.x
        y = self.y - other.y
        return Point(x, y)

    def __mul__(self, other):
        """
        Override + function for Point
        :param other: [Point] Point to add
        :return: [Point] Sum of self and other
        """
        x = self.x * other
        y = self.y * other
        return Point(x, y)

    def __eq__(self, other):
        """
        Override = function for Point
        :param other: [Point] Point to compare
        :return: [Bool] True if equal
        """
        return self.x == other.x and self.y == other.y


def Fp(k_p, q_r, q_k):
    return k_p * q_r.diff(q_k).norm()


def Foi(k_oi, q_r, q_oi, d_0):
    temp_diff = q_r.diff(q_oi)
    if temp_diff.norm() == 0:
        return math.inf

    if temp_diff.norm() <= d_0:
        F_oi = -k_oi * ((1 / (temp_diff.norm())) - (1 / d_0)) * (1 / math.pow(temp_diff.norm(), 2))
    else:
        F_oi = 0

    return F_oi


def creatingObstaclesList(obst_amount):
    Obstacles = []
    for i in range(obst_amount):
        Obstacles.append(Point(random.randrange(-SIZE, SIZE), random.randrange(-SIZE, SIZE)))
    return Obstacles


class PathFinder:
    """
    Class to find path
    """

    def __init__(self, _begin_position, _end_position, _obstacles_positions, _range=d_0):
        """
        Initialize Path Finder
        :param _begin_position: [Point] Starting point
        :param _end_position: [Point] Designation point
        :param _obstacles_positions: [Array of Points] Obstacle positions
        :param _range: [Double] Range of active obstacles
        """
        self.beginPosition = _begin_position
        self.endPosition = _end_position
        self.actualPosition = _begin_position
        self.obstaclesPositions = _obstacles_positions
        self.range = _range

    def next_step(self):
        """
        Find next step
        :return: next point
        """
        force = self.get_angle_factors(self.endPosition) * Fp(k_p, self.actualPosition, q_k)
        for obstacle in self.obstaclesPositions:
            force += self.get_angle_factors(obstacle) * Foi(k_oi, self.actualPosition, obstacle, self.range)

        logging.debug(f'Pos : {self.actualPosition.get()}\tforce: {force.get()}')
        direction = get_direction_vector(force)
        self.actualPosition += direction
        return

    def get_path(self):
        """
        Get path from beginning Point to the end Point
        :return: [matplotlib.path] Element with found path
        """
        vertices = [self.beginPosition.get()]
        codes = [Path.MOVETO]
        self.actualPosition = self.beginPosition
        while self.actualPosition != self.endPosition:
            self.next_step()
            vertices += [self.actualPosition.get()]
            codes += [Path.LINETO]
        path = Path(vertices, codes)
        return path

    def get_angle(self, point):
        """
        Get angle from actual position to given point
        :param point: [Point] Point to estimate angle to
        :return: [Double] Angle in radians
        """
        delta_point = point - self.actualPosition
        try:
            return np.arctan(delta_point.y/delta_point.x)
        except ZeroDivisionError:
            return delta_point.y/abs(delta_point.y)*np.pi/2

    def get_angle_factors(self, point):
        """
        Get force factors for x and y axis
        :param point: [Point] Point to estimate angle to
        :return: [Point] Factors of force
        """
        angle = self.get_angle(point)
        return Point(np.cos(angle), np.sin(angle))


def get_direction_vector(force):
    """
    Get next step vector
    :param force: [Point] Vector with forces in x and y axis
    :return: [Point] Direction vector
    """
    try:
        angle = np.arctan(force.y/force.x)
    except ZeroDivisionError:
        angle = force.y/abs(force.y)*np.pi/2
    direction = Point(0, 0)
    if -np.pi / 4 <= angle <= np.pi/4:
        direction += Point(delta, 0)
    elif angle > np.pi/4:
        direction += Point(0, delta)
    else:
        direction += Point(0, -delta)
    return direction


PunktPoczatkowy = Point(-SIZE, random.randrange(-SIZE, SIZE))
PunktKoncowy = Point(SIZE, random.randrange(-SIZE, SIZE))
Przeszkody = creatingObstaclesList(OBSTACLES_AMOUNT)

q_r = PunktPoczatkowy
q_k = PunktKoncowy

columnX = (2 * SIZE) / delta
columnY = (2 * SIZE) / delta
limitX = 2 * SIZE
limitY = 2 * SIZE

# Generowanie zerowej macierzy sił potencjalnych
width = int(columnX)
height = int(columnY)
potentialForcesMatrix = [[0] * width for i in range(height)]

for y in range(0, int(columnY)):
    for x in range(0, int(columnX)):
        wx = -SIZE + (x / columnX) * limitX
        wy = -SIZE + (y / columnY) * limitY
        q_r = Point(wx, wy)
        Suma_F_oi = 0
        for i in range(OBSTACLES_AMOUNT):
            q_oi = Przeszkody[i]
            Suma_F_oi += Foi(k_oi, q_r, q_oi, d_0)
        potentialForcesMatrix[y][x] = Fp(k_p, q_r, q_k) - Suma_F_oi

x = y = np.arange(-SIZE, SIZE, delta)
X, Y = np.meshgrid(x, y)
Z = potentialForcesMatrix

fig = plt.figure(figsize=(SIZE, SIZE))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalów')

# Add path to graph
pathFinder = PathFinder(PunktPoczatkowy, PunktKoncowy, Przeszkody)
path_patch = PathPatch(pathFinder.get_path(), facecolor='None', edgecolor='blue')
ax.add_patch(path_patch)

plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-SIZE, SIZE, -SIZE, SIZE])

# Rysuj punkt poczatkowy i koncowy
plt.plot(PunktPoczatkowy.x, PunktPoczatkowy.y, "or", color='blue')
plt.plot(PunktKoncowy.x, PunktKoncowy.y, "or", color='blue')

# Rysuj przeszkody
# for i in range(OBSTACLES_AMOUNT):
# plt.plot(Przeszkody[i].x, Przeszkody[i].y, "or", color='black')

# Rysuj plansze
plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
