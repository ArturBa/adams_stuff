Adam Krzykała 
Roboty mobilne 2019
Numer indeksu: 235 411

Odpowiedź na pytanie c zadania 2
Jaki wpływ mają parametry kp, koi, d0:

Parametr kp jest współczynnikiem wzmocnienia części siły w danym miejscu,
pochodzącej od potencjału przyciągającego. Zwiększanie współczynnika kp
ma wpływ na gradient pola, a w efekcie zwiększanie siły pochodzącej od
punktu końcowego, który przyciąga robota. Zwiększanie samego współczynnika kp,
bez zmian współczynnika koi powoduje, że zadanie dotarcia do celu otrzymuje rosnący priorytet 
w porównaniu do zadania omijania przeszkód. Zaniżenie tego spółczynnika spowoduje, że gradient 
potencjału pola zmaleje i robot będzie słabiej przyciągany przez cel, jednocześnie zwracając 
większą uwagę na omijanie przeszkód. 

Parametr koi występuje w części odpowiedzialnej za siły odpychania, czyli te które odciągają 
robota od przeszkód. Zwiększanie tego współczynnika powoduje większy gradient w momencie
wykrycia przeszkody, czyli wymusi szybsze zatrzymanie lub ominięcie przeszkody przez robota. 
Jego zmniejszenie z kolei powoduje, że priorytet omijania przeszkód maleje. 

Parametr d0 określa zasięg, na który przeszkoda oddziałuje. Jego zwiększanie powoduje zwiększanie
obszaru wokół robota, który poddany jest działaniu danej przeszkody. Można tym samym regulować 
wielkość strefy, na którą robot ma wpływ, a tym samym moment wykrycia przeszkody.
Im większy współczynnik d0, tym przeszkoda zostanie szybciej zauważona przez nadjeżdżającego robota.
Zmniejszanie może doprowadzić do sytuacji, w której robot nie zdąży się zatrzymać i uderzy 
w przeszkodę. 
